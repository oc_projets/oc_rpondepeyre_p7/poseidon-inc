# spring-boot

## Technical:

1. Framework: Spring Boot v3.0.5
2. Java 19
3. Thymeleaf
4. Bootstrap v.4.3.1

## Setup

1. Créer une base de données MySQL avec les informations specifiées dans le application.properties
2. Y executer le script contenu dans doc/data.sql
3. ouvrir le projet avec l'ide et y installer les dépendances ( mvn clean install )
4. Lancez le projet springboot
