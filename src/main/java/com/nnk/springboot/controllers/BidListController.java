package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.services.BidListService;

import jakarta.validation.Valid;

@Controller
public class BidListController {

    @Autowired
    BidListService bidService;

    private final Logger logger = Logger.getLogger(BidListController.class.getName());

    @GetMapping("/bidList/list")

    public String home(Model model) {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("bids", bidService.findAllBids());
        model.addAttribute("username", username);
        return "bidList/list";
    }

    @GetMapping("/bidList/add")
    public String addBidForm(BidList bid) {
        return "bidList/add";
    }

    @PostMapping("/bidList/validate")
    public String validate(@Valid BidList bid, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            bidService.saveBidList(bid);
            logger.info("Bid correctly added");
            model.addAttribute("bids", bidService.findAllBids());
            return "redirect:/bidList/list";
        }
        return "bidList/add";
    }

    @GetMapping("/bidList/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        BidList bid = bidService.findById(id);
        model.addAttribute("bidList", bid);
        return "bidList/update";
    }

    @PostMapping("/bidList/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid BidList bidList,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (!result.hasErrors()) {
            if (bidService.findById(id) != null) {
                bidList.setBidListId(id);
                bidService.saveBidList(bidList);
                logger.info("Bid " + id + " correctly saved");
                model.addAttribute("bids", bidService.findAllBids());
                return "redirect:/bidList/list";
            } else {
                throw new IllegalArgumentException("No Bid founded with id: " + id);
            }
        }
        return "bidList/update";
    }

    @GetMapping("/bidList/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id, Model model) {
        bidService.deleteBidList(id);
        logger.info("Bid " + id + " correctly deleted");
        model.addAttribute("bids", bidService.findAllBids());
        return "redirect:/bidList/list";
    }
}
