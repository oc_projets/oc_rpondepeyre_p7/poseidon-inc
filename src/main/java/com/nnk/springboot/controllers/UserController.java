package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.AuthenticationService;

import jakarta.validation.Valid;

@Controller
public class UserController {

    private final Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private AuthenticationService authService;

    @GetMapping("/user/list")
    public String home(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("users", authService.findAllUsers());
        model.addAttribute("username", username);
        return "user/list";
    }

    @GetMapping("/user/add")
    public String addUser(User user) {
        return "user/add";
    }

    @PostMapping("/user/validate")
    public String validate(@Valid User user, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setPassword(encoder.encode(user.getPassword()));
            authService.saveUser(user);
            logger.info("User correctly added");
            model.addAttribute("users", authService.findAllUsers());
            return "redirect:/user/list";
        }
        return "user/add";
    }

    @GetMapping("/user/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        User user = authService.findById(id);
        user.setPassword("");
        model.addAttribute("user", user);
        return "user/update";
    }

    @PostMapping("/user/update/{id}")
    public String updateUser(@PathVariable("id") Integer id, @Valid User user,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (result.hasErrors()) {
            return "user/update";
        }
        if (authService.findById(id) != null) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            user.setPassword(encoder.encode(user.getPassword()));
            user.setId(id);
            authService.saveUser(user);
            logger.info("User " + id + " correctly saved");
            model.addAttribute("users", authService.findAllUsers());
            return "redirect:/user/list";
        } else {
            throw new IllegalArgumentException("No User founded with id: " + id);
        }
    }

    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id, Model model) {
        authService.deleteUser(id);
        logger.info("User " + id + " correctly deleted");
        model.addAttribute("users", authService.findAllUsers());
        return "redirect:/user/list";
    }
}
