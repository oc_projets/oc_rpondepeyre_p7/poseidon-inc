package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.services.RatingService;

import jakarta.validation.Valid;

@Controller
public class RatingController {

    private final Logger logger = Logger.getLogger(RatingController.class.getName());

    @Autowired
    RatingService ratingService;

    @GetMapping("/rating/list")
    public String home(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("ratings", ratingService.findAllRatings());
        model.addAttribute("username", username);
        return "rating/list";
    }

    @GetMapping("/rating/add")
    public String addRatingForm(Rating rating) {
        return "rating/add";
    }

    @PostMapping("/rating/validate")
    public String validate(@Valid Rating rating, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            ratingService.saveRating(rating);
            logger.info("Rating correctly added");
            model.addAttribute("ratings", ratingService.findAllRatings());
            return "redirect:/rating/list";
        }
        return "rating/add";
    }

    @GetMapping("/rating/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        Rating rating = ratingService.findById(id);
        model.addAttribute("rating", rating);
        return "rating/update";
    }

    @PostMapping("/rating/update/{id}")
    public String updateRating(@PathVariable("id") Integer id, @Valid Rating rating,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (!result.hasErrors()) {
            if (ratingService.findById(id) != null) {
                rating.setId(id);
                ratingService.saveRating(rating);
                logger.info("Rating " + id + " correctly saved");
                model.addAttribute("ratings", ratingService.findAllRatings());
                return "redirect:/rating/list";
            } else {
                throw new IllegalArgumentException("No Rating founded with id: " + id);
            }
        }
        return "rating/update";
    }

    @GetMapping("/rating/delete/{id}")
    public String deleteRating(@PathVariable("id") Integer id, Model model) {
        ratingService.deleteRating(id);
        logger.info("Rating " + id + " correctly deleted");
        model.addAttribute("ratings", ratingService.findAllRatings());
        return "redirect:/rating/list";
    }
}
