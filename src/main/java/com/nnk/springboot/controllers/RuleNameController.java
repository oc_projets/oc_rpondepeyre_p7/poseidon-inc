package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.services.RuleNameService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;

@Controller
public class RuleNameController {

    private final Logger logger = Logger.getLogger(RuleNameController.class.getName());

    @Autowired
    HttpServletRequest req;

    @Autowired
    RuleNameService ruleNameService;

    @GetMapping("/ruleName/list")
    public String home(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("rulenames", ruleNameService.findAllRules());
        model.addAttribute("username", username);
        return "ruleName/list";
    }

    @GetMapping("/ruleName/add")
    public String addRuleForm(RuleName bid) {
        return "ruleName/add";
    }

    @PostMapping("/ruleName/validate")
    public String validate(@Valid RuleName ruleName, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            ruleNameService.saveRuleName(ruleName);
            logger.info("RuleName correctly added");
            model.addAttribute("rulenames", ruleNameService.findAllRules());
            return "redirect:/ruleName/list";
        }
        return "ruleName/add";
    }

    @GetMapping("/ruleName/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        RuleName rulename = ruleNameService.findById(id);
        model.addAttribute("ruleName", rulename);
        return "ruleName/update";
    }

    @PostMapping("/ruleName/update/{id}")
    public String updateRuleName(@PathVariable("id") Integer id, @Valid RuleName ruleName,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (!result.hasErrors()) {
            if (ruleNameService.findById(id) != null) {
                ruleName.setId(id);
                ruleNameService.saveRuleName(ruleName);
                logger.info("RuleName " + id + " correctly saved");
                model.addAttribute("rulenames", ruleNameService.findAllRules());
                return "redirect:/ruleName/list";
            } else {
                throw new IllegalArgumentException("No RuleName founded with id: " + id);
            }
        }
        return "ruleName/update";
    }

    @GetMapping("/ruleName/delete/{id}")
    public String deleteRuleName(@PathVariable("id") Integer id, Model model) {
        ruleNameService.deleteRuleName(id);
        logger.info("RuleName " + id + " correctly deleted");
        model.addAttribute("rulenames", ruleNameService.findAllRules());
        return "redirect:/ruleName/list";
    }
}
