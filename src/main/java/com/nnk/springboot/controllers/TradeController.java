package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.services.TradeService;

import jakarta.validation.Valid;

@Controller
public class TradeController {

    private final Logger logger = Logger.getLogger(TradeController.class.getName());

    @Autowired
    TradeService tradeService;

    @GetMapping("/trade/list")
    public String home(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("trades", tradeService.findAllTrades());
        model.addAttribute("username", username);
        return "trade/list";
    }

    @GetMapping("/trade/add")
    public String addTrade(Trade bid) {
        return "trade/add";
    }

    @PostMapping("/trade/validate")
    public String validate(@Valid Trade trade, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            tradeService.saveTrade(trade);
            logger.info("Trade correctly added");
            model.addAttribute("trades", tradeService.findAllTrades());
            return "redirect:/trade/list";
        }
        return "trade/add";
    }

    @GetMapping("/trade/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        Trade trade = tradeService.findById(id);
        model.addAttribute("trade", trade);
        return "trade/update";
    }

    @PostMapping("/trade/update/{id}")
    public String updateTrade(@PathVariable("id") Integer id, @Valid Trade trade,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (!result.hasErrors()) {
            if (tradeService.findById(id) != null) {
                trade.setTradeId(id);
                tradeService.saveTrade(trade);
                logger.info("Trade " + id + " correctly saved");
                model.addAttribute("trades", tradeService.findAllTrades());
                return "redirect:/trade/list";
            } else {
                throw new IllegalArgumentException("No Trade founded with id: " + id);
            }
        }
        return "trade/update";
    }

    @GetMapping("/trade/delete/{id}")
    public String deleteTrade(@PathVariable("id") Integer id, Model model) {
        tradeService.deleteTrade(id);
        logger.info("Trade " + id + " correctly deleted");
        model.addAttribute("trades", tradeService.findAllTrades());
        return "redirect:/trade/list";
    }
}
