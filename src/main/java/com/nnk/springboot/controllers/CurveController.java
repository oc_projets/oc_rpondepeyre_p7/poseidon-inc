package com.nnk.springboot.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.services.CurveService;

import jakarta.validation.Valid;

@Controller
public class CurveController {

    private final Logger logger = Logger.getLogger(CurveController.class.getName());

    @Autowired
    CurveService curveService;

    @GetMapping("/curvePoint/list")
    public String home(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("curves", curveService.findAllCurves());
        model.addAttribute("username", username);
        return "curvePoint/list";
    }

    @GetMapping("/curvePoint/add")
    public String addCurveForm(CurvePoint curve) {
        return "curvePoint/add";
    }

    @PostMapping("/curvePoint/validate")
    public String validate(@Valid CurvePoint curvePoint, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            curveService.saveCurve(curvePoint);
            logger.info("Curve correctly added");
            model.addAttribute("curves", curveService.findAllCurves());
            return "redirect:/curvePoint/list";
        }
        return "curvePoint/add";
    }

    @GetMapping("/curvePoint/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        CurvePoint curve = curveService.findById(id);
        model.addAttribute("curvePoint", curve);
        return "curvePoint/update";
    }

    @PostMapping("/curvePoint/update/{id}")
    public String updateCurve(@PathVariable("id") Integer id, @Valid CurvePoint curvePoint,
            BindingResult result, Model model) throws IllegalArgumentException {
        if (!result.hasErrors()) {
            if (curveService.findById(id) != null) {
                curvePoint.setId(id);
                curveService.saveCurve(curvePoint);
                logger.info("Curve " + id + " correctly saved");
                model.addAttribute("curves", curveService.findAllCurves());
                return "redirect:/curvePoint/list";
            } else {
                throw new IllegalArgumentException("No Curve founded with id: " + id);
            }
        }
        return "curvePoint/update";
    }

    @GetMapping("/curvePoint/delete/{id}")
    public String deleteCurve(@PathVariable("id") Integer id, Model model) {
        curveService.deleteCurve(id);
        logger.info("Curve " + id + " correctly deleted");
        model.addAttribute("curves", curveService.findAllCurves());
        return "redirect:/curvePoint/list";
    }
}
