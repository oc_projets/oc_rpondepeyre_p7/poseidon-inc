package com.nnk.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;

@Service
public class CurveService {

    @Autowired
    CurvePointRepository repository;

    public List<CurvePoint> findAllCurves() {
        return repository.findAll();
    }

    public CurvePoint findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid curvePoint Id:" + id));
    }

    public CurvePoint saveCurve(CurvePoint curve) {
        return repository.save(curve);
    }

    public void deleteCurve(Integer id) {
        CurvePoint curve = findById(id);
        repository.delete(curve);
    }
}
