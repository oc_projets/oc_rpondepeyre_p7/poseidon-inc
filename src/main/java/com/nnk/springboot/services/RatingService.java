package com.nnk.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.RatingRepository;

@Service
public class RatingService {

    @Autowired
    RatingRepository repository;

    public List<Rating> findAllRatings() {
        return repository.findAll();
    }

    public Rating findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid rating Id:" + id));
    }

    public Rating saveRating(Rating rating) {
        return repository.save(rating);
    }

    public void deleteRating(Integer id) {
        Rating rating = findById(id);
        repository.delete(rating);
    }
}
