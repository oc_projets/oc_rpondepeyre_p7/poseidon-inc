package com.nnk.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.TradeRepository;

@Service
public class TradeService {

    @Autowired
    TradeRepository repository;

    public List<Trade> findAllTrades() {
        return repository.findAll();
    }

    public Trade findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid trade Id:" + id));
    }

    public Trade saveTrade(Trade trade) {
        return repository.save(trade);
    }

    public void deleteTrade(Integer id) {
        Trade trade = findById(id);
        repository.delete(trade);
    }

}
