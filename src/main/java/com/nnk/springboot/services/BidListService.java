package com.nnk.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;

@Service
public class BidListService {

    @Autowired
    BidListRepository repository;

    public List<BidList> findAllBids() {
        return repository.findAll();
    }

    public BidList findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid bidList Id:" + id));
    }

    public BidList saveBidList(BidList bid) {
        return repository.save(bid);
    }

    public void deleteBidList(Integer id) {
        BidList bid = findById(id);
        repository.delete(bid);
    }
}
