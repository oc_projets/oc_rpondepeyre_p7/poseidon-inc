package com.nnk.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.RuleNameRepository;

@Service
public class RuleNameService {

    @Autowired
    RuleNameRepository repository;

    public List<RuleName> findAllRules() {
        return repository.findAll();
    }

    public RuleName findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid ruleName Id:" + id));
    }

    public RuleName saveRuleName(RuleName rule) {
        return repository.save(rule);
    }

    public void deleteRuleName(Integer id) {
        RuleName rule = findById(id);
        repository.delete(rule);
    }
}
