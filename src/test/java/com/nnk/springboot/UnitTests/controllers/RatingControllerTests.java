package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.RatingController;
import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.RatingService;

@ExtendWith(MockitoExtension.class)
public class RatingControllerTests {

    @Mock
    RatingService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    RatingController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("USER");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllRatings();
    }

    @Test
    public void validate_ok() {
        Rating rating = new Rating();
        when(result.hasErrors()).thenReturn(false);
        controller.validate(rating, result, model);
        verify(service, Mockito.times(1)).saveRating(rating);
    }

    @Test
    public void validate_error() {
        Rating rating = new Rating();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(rating, result, model);
        verify(service, Mockito.times(0)).saveRating(rating);
    }

    @Test
    public void updateRating_ok() throws Exception {
        Rating rating = new Rating();
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(rating);
        controller.updateRating(1, rating, result, model);
        verify(service, Mockito.times(1)).saveRating(rating);
    }

    @Test
    public void updateRating_error() throws Exception {
        Rating rating = new Rating();
        when(result.hasErrors()).thenReturn(true);
        controller.updateRating(1, rating, result, model);
        verify(service, Mockito.times(0)).saveRating(rating);
    }

    @Test
    public void updateRating_Notfoundid() {
        Rating rating = new Rating();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateRating(1, rating, result, model);
        });
    }

    @Test
    public void deleteRating() {
        controller.deleteRating(1, model);
        verify(service, Mockito.times(1)).deleteRating(1);
    }
}
