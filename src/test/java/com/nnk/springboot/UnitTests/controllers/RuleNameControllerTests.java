package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.RuleNameController;
import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.RuleNameService;

@ExtendWith(MockitoExtension.class)
public class RuleNameControllerTests {

    @Mock
    RuleNameService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    RuleNameController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("USER");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllRules();
    }

    @Test
    public void validate_ok() {
        RuleName rule = new RuleName();
        when(result.hasErrors()).thenReturn(false);
        controller.validate(rule, result, model);
        verify(service, Mockito.times(1)).saveRuleName(rule);
    }

    @Test
    public void validate_error() {
        RuleName rule = new RuleName();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(rule, result, model);
        verify(service, Mockito.times(0)).saveRuleName(rule);
    }

    @Test
    public void updateRule_ok() throws Exception {
        RuleName rule = new RuleName();
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(rule);
        controller.updateRuleName(1, rule, result, model);
        verify(service, Mockito.times(1)).saveRuleName(rule);
    }

    @Test
    public void updateRule_error() throws Exception {
        RuleName rule = new RuleName();
        when(result.hasErrors()).thenReturn(true);
        controller.updateRuleName(1, rule, result, model);
        verify(service, Mockito.times(0)).saveRuleName(rule);
    }

    @Test
    public void updateRule_Notfoundid() {
        RuleName rule = new RuleName();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateRuleName(1, rule, result, model);
        });
    }

    @Test
    public void deleteRule() {
        controller.deleteRuleName(1, model);
        verify(service, Mockito.times(1)).deleteRuleName(1);
    }
}
