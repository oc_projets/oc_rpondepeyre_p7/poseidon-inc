package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.CurveController;
import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.CurveService;

@ExtendWith(MockitoExtension.class)
public class CurveControllerTests {

    @Mock
    CurveService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    CurveController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("USER");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllCurves();
    }

    @Test
    public void validate_ok() {
        CurvePoint curve = new CurvePoint();
        when(result.hasErrors()).thenReturn(false);
        controller.validate(curve, result, model);
        verify(service, Mockito.times(1)).saveCurve(curve);
    }

    @Test
    public void validate_error() {
        CurvePoint curve = new CurvePoint();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(curve, result, model);
        verify(service, Mockito.times(0)).saveCurve(curve);
    }

    @Test
    public void updateCurve_ok() throws Exception {
        CurvePoint curve = new CurvePoint();
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(curve);
        controller.updateCurve(1, curve, result, model);
        verify(service, Mockito.times(1)).saveCurve(curve);
    }

    @Test
    public void updateCurve_error() throws Exception {
        CurvePoint curve = new CurvePoint();
        when(result.hasErrors()).thenReturn(true);
        controller.updateCurve(1, curve, result, model);
        verify(service, Mockito.times(0)).saveCurve(curve);
    }

    @Test
    public void updateCurve_Notfoundid() {
        CurvePoint curve = new CurvePoint();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateCurve(1, curve, result, model);
        });
    }

    @Test
    public void deleteCurve() {
        controller.deleteCurve(1, model);
        verify(service, Mockito.times(1)).deleteCurve(1);
    }
}
