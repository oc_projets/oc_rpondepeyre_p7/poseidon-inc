package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.TradeController;
import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.TradeService;

@ExtendWith(MockitoExtension.class)
public class TradeControllerTests {

    @Mock
    TradeService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    TradeController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("USER");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllTrades();
    }

    @Test
    public void validate_ok() {
        Trade trade = new Trade();
        when(result.hasErrors()).thenReturn(false);
        controller.validate(trade, result, model);
        verify(service, Mockito.times(1)).saveTrade(trade);
    }

    @Test
    public void validate_error() {
        Trade trade = new Trade();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(trade, result, model);
        verify(service, Mockito.times(0)).saveTrade(trade);
    }

    @Test
    public void updateTrade_ok() throws Exception {
        Trade trade = new Trade();
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(trade);
        controller.updateTrade(1, trade, result, model);
        verify(service, Mockito.times(1)).saveTrade(trade);
    }

    @Test
    public void updateTrade_error() throws Exception {
        Trade trade = new Trade();
        when(result.hasErrors()).thenReturn(true);
        controller.updateTrade(1, trade, result, model);
        verify(service, Mockito.times(0)).saveTrade(trade);
    }

    @Test
    public void updateTrade_Notfoundid() {
        Trade trade = new Trade();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateTrade(1, trade, result, model);
        });
    }

    @Test
    public void deleteTrade() {
        controller.deleteTrade(1, model);
        verify(service, Mockito.times(1)).deleteTrade(1);
    }
}
