package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.UserController;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.AuthenticationService;

@ExtendWith(MockitoExtension.class)
public class UserControllerTests {

    @Mock
    AuthenticationService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    UserController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("ADMIN");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllUsers();
    }

    @Test
    public void validate_ok() {
        User user = new User();
        user.setPassword("null");
        when(result.hasErrors()).thenReturn(false);
        controller.validate(user, result, model);
        verify(service, Mockito.times(1)).saveUser(user);
    }

    @Test
    public void validate_error() {
        User user = new User();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(user, result, model);
        verify(service, Mockito.times(0)).saveUser(user);
    }

    @Test
    public void updateUser_ok() throws Exception {
        User user = new User();
        user.setPassword("null");
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(user);
        controller.updateUser(1, user, result, model);
        verify(service, Mockito.times(1)).saveUser(user);
    }

    @Test
    public void updateUser_error() throws Exception {
        User user = new User();
        when(result.hasErrors()).thenReturn(true);
        controller.updateUser(1, user, result, model);
        verify(service, Mockito.times(0)).saveUser(user);
    }

    @Test
    public void updateUser_Notfoundid() {
        User user = new User();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateUser(1, user, result, model);
        });
    }

    @Test
    public void deleteBid() {
        controller.deleteUser(1, model);
        verify(service, Mockito.times(1)).deleteUser(1);
    }
}
