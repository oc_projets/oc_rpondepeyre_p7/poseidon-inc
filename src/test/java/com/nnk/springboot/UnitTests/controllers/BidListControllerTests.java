package com.nnk.springboot.UnitTests.controllers;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.nnk.springboot.controllers.BidListController;
import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.BidListService;

@ExtendWith(MockitoExtension.class)
public class BidListControllerTests {

    @Mock
    BidListService service;

    @Mock
    SecurityContext securityContext;

    @Mock
    Model model;

    @Mock
    BindingResult result;

    @InjectMocks
    BidListController controller;

    Authentication authentication;

    @BeforeEach
    void setup() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setRole("USER");
        this.authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        SecurityContextHolder.setContext(securityContext);

    }

    @Test
    public void home() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        controller.home(model);
        verify(service).findAllBids();
    }

    @Test
    public void validate_ok() {
        BidList bid = new BidList();
        when(result.hasErrors()).thenReturn(false);
        controller.validate(bid, result, model);
        verify(service, Mockito.times(1)).saveBidList(bid);
    }

    @Test
    public void validate_error() {
        BidList bid = new BidList();
        when(result.hasErrors()).thenReturn(true);
        controller.validate(bid, result, model);
        verify(service, Mockito.times(0)).saveBidList(bid);
    }

    @Test
    public void updateBid_ok() throws Exception {

        BidList bid = new BidList();
        when(result.hasErrors()).thenReturn(false);
        when(service.findById(1)).thenReturn(bid);
        controller.updateBid(1, bid, result, model);
        verify(service, Mockito.times(1)).saveBidList(bid);
    }

    @Test
    public void updateBid_error() throws Exception {
        BidList bid = new BidList();
        when(result.hasErrors()).thenReturn(true);
        controller.updateBid(1, bid, result, model);
        verify(service, Mockito.times(0)).saveBidList(bid);
    }

    @Test
    public void updateBid_Notfoundid() {
        BidList bid = new BidList();
        when(result.hasErrors()).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> {
            controller.updateBid(1, bid, result, model);
        });
    }

    @Test
    public void deleteBid() {
        controller.deleteBid(1, model);
        verify(service, Mockito.times(1)).deleteBidList(1);
    }
}
