package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.repositories.UserRepository;
import com.nnk.springboot.services.AuthenticationService;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTests {

    @Mock
    UserRepository repository;

    @InjectMocks
    AuthenticationService service;

    @Test
    void findAllUsers() {

        service.findAllUsers();
        verify(repository).findAll();
    }

    @Test
    void findByIdTrue() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setId(1);
        user.setFullname("fullName");
        user.setRole("USER");

        when(repository.findById(1)).thenReturn(Optional.of(user));
        User result = service.findById(1);
        verify(repository).findById(1);
        assertEquals("fullName", result.getFullname());
    }

    @Test
    void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid user Id:1", thrown.getMessage());
    }

    @Test
    void saveUser() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setId(1);
        user.setFullname("fullName");
        user.setRole("USER");

        service.saveUser(user);
        verify(repository).save(user);

    }

    @Test
    void deleteUser() {

        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setId(1);
        user.setFullname("fullName");
        user.setRole("USER");

        when(repository.findById(1)).thenReturn(Optional.of(user));
        service.deleteUser(1);
        verify(repository).delete(user);
    }

    @Test
    void loadUserByUsernameTrue() {

        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setId(1);
        user.setFullname("fullName");
        user.setRole("USER");

        when(repository.findByUsername("username")).thenReturn(Optional.of(user));

        UserDetails result = service.loadUserByUsername("username");

        assertEquals("username", result.getUsername());
        assertEquals("password", result.getPassword());
        assertEquals(Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")), result.getAuthorities());

    }

    @Test
    void loadUserByUsernameFalse() {

        when(repository.findByUsername("username")).thenReturn(Optional.empty());

        UsernameNotFoundException thrown = assertThrows(UsernameNotFoundException.class, () -> service.loadUserByUsername("username") );
        assertEquals("The requested account could not be found", thrown.getMessage());
    }

}
