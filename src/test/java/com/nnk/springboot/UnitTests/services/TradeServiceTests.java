package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.TradeRepository;
import com.nnk.springboot.services.TradeService;

@ExtendWith(MockitoExtension.class)
public class TradeServiceTests {

    @Mock
    TradeRepository repository;

    @InjectMocks
    TradeService service;

    @Test
    public void findAllTradesTests() {

        service.findAllTrades();
        verify(repository).findAll();
    }

    @Test
    public void findByIdTrue() {
        Trade trade = new Trade("account", "type");
        when(repository.findById(1)).thenReturn(Optional.of(trade));
        Trade result = service.findById(1);
        verify(repository).findById(1);
        assertEquals("account", result.getAccount());
    }

    @Test
    public void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid trade Id:1", thrown.getMessage());
    }

    @Test
    public void saveTrade() {
        Trade trade = new Trade("account", "type");

        service.saveTrade(trade);
        verify(repository).save(trade);

    }

    @Test
    public void deleteTrade() {
        Trade trade = new Trade("account", "type");
        when(repository.findById(1)).thenReturn(Optional.of(trade));
        service.deleteTrade(1);
        verify(repository).delete(trade);
    }

}
