package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.RuleNameRepository;
import com.nnk.springboot.services.RuleNameService;

@ExtendWith(MockitoExtension.class)
public class RuleNameServiceTests {

    @Mock
    RuleNameRepository repository;

    @InjectMocks
    RuleNameService service;

    @Test
    public void findAllRulesTests() {

        service.findAllRules();
        verify(repository).findAll();
    }

    @Test
    public void findByIdTrue() {
        RuleName rule = new RuleName("name", "description", "json", "template", "sqlStr", "sqlPart");
        when(repository.findById(1)).thenReturn(Optional.of(rule));
        RuleName result = service.findById(1);
        verify(repository).findById(1);
        assertEquals("name", result.getName());
    }

    @Test
    public void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid ruleName Id:1", thrown.getMessage());
    }

    @Test
    public void saveRule() {
        RuleName rule = new RuleName("name", "description", "json", "template", "sqlStr", "sqlPart");

        service.saveRuleName(rule);
        verify(repository).save(rule);

    }

    @Test
    public void deleteRule() {
        RuleName rule = new RuleName("name", "description", "json", "template", "sqlStr", "sqlPart");

        when(repository.findById(1)).thenReturn(Optional.of(rule));
        service.deleteRuleName(1);
        verify(repository).delete(rule);
    }

}
