package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.RatingRepository;
import com.nnk.springboot.services.RatingService;

@ExtendWith(MockitoExtension.class)
public class RatingServiceTests {

    @Mock
    RatingRepository repository;

    @InjectMocks
    RatingService service;

    @Test
    public void findAllRatingsTests() {

        service.findAllRatings();
        verify(repository).findAll();
    }

    @Test
    public void findByIdTrue() {
        Rating rating = new Rating("mood", "sand", "fitch", 1);
        when(repository.findById(1)).thenReturn(Optional.of(rating));
        Rating result = service.findById(1);
        verify(repository).findById(1);
        assertEquals("mood", result.getMoodysRating());
    }

    @Test
    public void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid rating Id:1", thrown.getMessage());
    }

    @Test
    public void saveRating() {
        Rating rating = new Rating("mood", "sand", "fitch", 1);

        service.saveRating(rating);
        verify(repository).save(rating);

    }

    @Test
    public void deleteRating() {
        Rating rating = new Rating("mood", "sand", "fitch", 1);
        when(repository.findById(1)).thenReturn(Optional.of(rating));
        service.deleteRating(1);
        verify(repository).delete(rating);
    }

}
