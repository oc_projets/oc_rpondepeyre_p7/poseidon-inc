package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;
import com.nnk.springboot.services.BidListService;

@ExtendWith(MockitoExtension.class)
public class BidListServiceTests {

    @Mock
    BidListRepository repository;

    @InjectMocks
    BidListService service;

    @Test
    public void findAllBidsTests() {

        service.findAllBids();
        verify(repository).findAll();
    }

    @Test
    public void findByIdTrue() {
        BidList bid = new BidList("account", "type", 0d);
        when(repository.findById(1)).thenReturn(Optional.of(bid));
        BidList result = service.findById(1);
        verify(repository).findById(1);
        assertEquals("account", result.getAccount());
    }

    @Test
    public void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid bidList Id:1", thrown.getMessage());
    }

    @Test
    public void saveBidList() {
        BidList bid = new BidList("account", "type", 0d);

        service.saveBidList(bid);
        verify(repository).save(bid);

    }

    @Test
    public void deleteBidList() {
        BidList bid = new BidList("account", "type", 0d);
        when(repository.findById(1)).thenReturn(Optional.of(bid));
        service.deleteBidList(1);
        verify(repository).delete(bid);
    }

}
