package com.nnk.springboot.UnitTests.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;
import com.nnk.springboot.services.CurveService;

@ExtendWith(MockitoExtension.class)
public class CurveServiceTests {

    @Mock
    CurvePointRepository repository;

    @InjectMocks
    CurveService service;

    @Test
    public void findAllCurvesTests() {

        service.findAllCurves();
        verify(repository).findAll();
    }

    @Test
    public void findByIdTrue() {
        CurvePoint curve = new CurvePoint(1, 0d, 0d);
        when(repository.findById(1)).thenReturn(Optional.of(curve));
        CurvePoint result = service.findById(1);
        verify(repository).findById(1);
        assertEquals(0d, result.getTerm());
    }

    @Test
    public void findByIdFalse() {

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.findById(1));
        assertEquals("Invalid curvePoint Id:1", thrown.getMessage());
    }

    @Test
    public void saveCurve() {
        CurvePoint curve = new CurvePoint(1, 0d, 0d);

        service.saveCurve(curve);
        verify(repository).save(curve);

    }

    @Test
    public void deleteCurve() {
        CurvePoint curve = new CurvePoint(1, 0d, 0d);
        when(repository.findById(1)).thenReturn(Optional.of(curve));
        service.deleteCurve(1);
        verify(repository).delete(curve);
    }

}
