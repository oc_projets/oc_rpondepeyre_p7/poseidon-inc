package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.services.CurveService;

@SpringBootTest
public class CurvePointTests {

	@Autowired
	private CurveService curveService;

	@Test
	public void curvePointTest() {
		CurvePoint curvePoint = new CurvePoint(10, 10d, 30d);

		// Save
		curvePoint = curveService.saveCurve(curvePoint);
		assertNotNull(curvePoint.getId());
		assertEquals(10, curvePoint.getCurveId());

		// Update
		curvePoint.setCurveId(20);
		curvePoint = curveService.saveCurve(curvePoint);
		assertEquals(20, curvePoint.getCurveId());

		// Find
		List<CurvePoint> listResult = curveService.findAllCurves();
		assertTrue(listResult.size() > 0);

		CurvePoint result = curveService.findById(curvePoint.getId());
		assertEquals(curvePoint.getCurveId(), result.getCurveId());

		// Delete
		Integer id = curvePoint.getId();
		curveService.deleteCurve(id);
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
				() -> curveService.findById(id));
		assertEquals("Invalid curvePoint Id:" + id, thrown.getMessage());

	}

}
