package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.services.RatingService;

@SpringBootTest
public class RatingTests {

	@Autowired
	private RatingService ratingService;

	@Test
	public void ratingTest() {
		Rating rating = new Rating("Moodys Rating", "Sand PRating", "Fitch Rating", 10);

		// Save
		rating = ratingService.saveRating(rating);
		assertNotNull(rating.getId());
		assertEquals(10, rating.getOrderNumber());

		// Update
		rating.setOrderNumber(20);
		rating = ratingService.saveRating(rating);
		assertEquals(20, rating.getOrderNumber());

		// Find
		List<Rating> listResult = ratingService.findAllRatings();
		assertTrue(listResult.size() > 0);

		Rating result = ratingService.findById(rating.getId());
		assertEquals(rating.getOrderNumber(), result.getOrderNumber());

		// Delete
		Integer id = rating.getId();
		ratingService.deleteRating(id);
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
				() -> ratingService.findById(id));
		assertEquals("Invalid rating Id:" + id, thrown.getMessage());

	}
}
