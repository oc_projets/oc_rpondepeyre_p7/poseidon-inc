package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.services.BidListService;

@SpringBootTest
public class BidTests {

	@Autowired
	private BidListService bidListService;

	@Test
	public void bidListTest() {
		BidList bid = new BidList("Account Test", "Type Test", 10d);

		// Save
		bid = bidListService.saveBidList(bid);
		assertNotNull(bid.getBidListId());
		assertEquals(10d, bid.getBidQuantity());

		// Update
		bid.setBidQuantity(20d);
		bid = bidListService.saveBidList(bid);
		assertEquals(20d, bid.getBidQuantity());

		// Find
		List<BidList> listResult = bidListService.findAllBids();
		assertTrue(listResult.size() > 0);

		BidList result = bidListService.findById(bid.getBidListId());
		assertEquals(bid.getBidQuantity(), result.getBidQuantity());

		// Delete
		Integer id = bid.getBidListId();
		bidListService.deleteBidList(id);
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
				() -> bidListService.findById(id));
		assertEquals("Invalid bidList Id:" + id, thrown.getMessage());
	}

}