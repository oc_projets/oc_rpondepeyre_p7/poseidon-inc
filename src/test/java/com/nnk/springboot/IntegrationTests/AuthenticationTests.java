package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.services.AuthenticationService;

@SpringBootTest
public class AuthenticationTests {

    @Autowired
    private AuthenticationService authService;

    @Test
    public void authenticationTest() {
        User user = new User();
        user.setFullname("Fullname");
        user.setUsername("Username");
        user.setPassword("Password.1");
        user.setRole("USER");

        // Save
        user = authService.saveUser(user);
        assertNotNull(user.getId());
        assertEquals("Fullname", user.getFullname());

        // Update
        user.setFullname("Fullname Update");
        user = authService.saveUser(user);
        assertEquals("Fullname Update", user.getFullname());

        // Find
        List<User> listResult = authService.findAllUsers();
        assertTrue(listResult.size() > 0);

        User result = authService.findById(user.getId());
        assertEquals(user.getFullname(), result.getFullname());

        // Authenticate
        UserDetails authenticated = authService.loadUserByUsername(user.getUsername());

        assertEquals(Collections.singleton(new SimpleGrantedAuthority("ROLE_USER")),
                authenticated.getAuthorities());
        assertEquals("Username", authenticated.getUsername());

        // Delete
        Integer id = user.getId();
        authService.deleteUser(id);
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
                () -> authService.findById(id));
        assertEquals("Invalid user Id:" + id, thrown.getMessage());
    }

}
