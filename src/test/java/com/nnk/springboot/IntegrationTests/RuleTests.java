package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.services.RuleNameService;

@SpringBootTest
public class RuleTests {

	@Autowired
	private RuleNameService ruleNameService;

	@Test
	public void ruleTest() {
		RuleName rule = new RuleName("Rule Name", "Description", "Json", "Template", "SQL", "SQL Part");

		// Save
		rule = ruleNameService.saveRuleName(rule);
		assertNotNull(rule.getId());
		assertEquals("Rule Name", rule.getName());

		// Update
		rule.setName("Rule Name Update");
		rule = ruleNameService.saveRuleName(rule);
		assertEquals("Rule Name Update", rule.getName());

		// Find
		List<RuleName> listResult = ruleNameService.findAllRules();
		assertTrue(listResult.size() > 0);

		RuleName result = ruleNameService.findById(rule.getId());
		assertEquals(rule.getName(), result.getName());

		// Delete
		Integer id = rule.getId();
		ruleNameService.deleteRuleName(id);
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
				() -> ruleNameService.findById(id));
		assertEquals("Invalid ruleName Id:" + id, thrown.getMessage());

	}
}
