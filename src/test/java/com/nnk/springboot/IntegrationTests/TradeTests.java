package com.nnk.springboot.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.services.TradeService;

@SpringBootTest
public class TradeTests {

	@Autowired
	private TradeService tradeService;

	@Test
	public void tradeTest() {
		Trade trade = new Trade("Trade Account", "Type");

		// Save
		trade = tradeService.saveTrade(trade);
		assertNotNull(trade.getTradeId());
		assertEquals("Trade Account", trade.getAccount());

		// Update
		trade.setAccount("Trade Account Update");
		trade = tradeService.saveTrade(trade);
		assertEquals("Trade Account Update", trade.getAccount());

		// Find
		List<Trade> listResult = tradeService.findAllTrades();
		assertTrue(listResult.size() > 0);

		Trade result = tradeService.findById(trade.getTradeId());
		assertEquals(trade.getAccount(), result.getAccount());

		// Delete
		Integer id = trade.getTradeId();
		tradeService.deleteTrade(id);
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
				() -> tradeService.findById(id));
		assertEquals("Invalid trade Id:" + id, thrown.getMessage());
	}
}
